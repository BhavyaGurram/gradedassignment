package com.org.Week2GradedAssignment;

public class Employee {
	int id;
	String name;
	int age;
	int salary;//per annum
	String department;
	String city;
	
		
	public int getId() {
		return id;
	}
	public void setId(int id) {

		if(id<0) {
			throw new IllegalArgumentException ("id can not less than 0 ");
		}
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name==null) {
			throw new IllegalArgumentException ("name cannot be null");
		}
		this.name = name;
		
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		if(age<0) {
			throw new IllegalArgumentException ("age can not less than o");
			}
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		if(salary<0) {
			throw new IllegalArgumentException ("salary can not less than o");
			}
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		if(department==null) {
			throw new IllegalArgumentException ("department cannot be null");
		}
		
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		if(city==null) {
			throw new IllegalArgumentException ("city cannot be null");
		}
		this.city = city;
	}
	public void EmployeeDetails(){
		System.out.println(id + " " + name + " " + age + " " + salary + " " + department + " " + city);
		
	}
	
	
	
	

}
