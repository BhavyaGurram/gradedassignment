package com.org.Week2GradedAssignment;


import java.util.ArrayList;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> employees = new ArrayList<Employee>();
		System.out.println("List of employees");
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setName("Aman");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");
		e1.EmployeeDetails();
		employees.add(e1);
		
		Employee e2 = new Employee();
		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("HR");
		e2.setCity("Bombay");
		e2.EmployeeDetails();
		employees.add(e2);
		
		Employee e3 = new Employee();
		e3.setId(3);
		e3.setName("Zoe");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");
		e3.EmployeeDetails();
		employees.add(e3);
	
		Employee e4 = new Employee();
		e4.setId(4);
		e4.setName("Smitha");
		e4.setAge(21);
		e4.setSalary(1000000);
		e4.setDepartment("IT");
		e4.setCity("Chennai");
	    e4.EmployeeDetails();
	    employees.add(e4);
		
		Employee e5 = new Employee();
		e5.setId(5);
		e5.setName("Smitha");
		e5.setAge(24);
		e5.setSalary(1200000);
		e5.setDepartment("HR");
		e5.setCity("Bengaluru");
		e5.EmployeeDetails();
		employees.add(e5);
		
		
		DataStructureA a = new DataStructureA();
		a.sortingNames(employees);
		
		DataStructureB b = new DataStructureB();
		b.cityNameCount(employees);
		System.out.println("");
		b.monthlySalary(employees);
		
		
		
 
	}

}
